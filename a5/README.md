# LIS 4369 - Extensible Enterprise Solutions
----
## jdw19g / James White
----
### Assignment 5 Requirements:

1. Complete the Introduction to R setup and the tutorial.
2. Code demo1.R and demo2.R to create lis4369_a5.R.
3. Using the RStudio environment code and run lis4369_a5.R.
4. Skill set 13: Sphere Volume Calculator.
5. Skill set 14: Calculator with Error Handling.
6. Skill set 15: Writing/ Reading to a File.
7. Provide screenshots of all work.
8. Bitbucket Repo Links:
    
    a) https://bitbucket.org/jdw19g/lis4369

#### README.md file should include the following items:

* Screenshot of lis4369_a5.R running in RStudio.

* Screenshots of skill sets.

#### Assignment Screenshots:
----
### lis4369_a5.R:

*lis4369_a5.R RStudio Environment*: ![RStudio environment](img/r_env.png)

| | |
|---|---|
| *Code part 1*: ![1](img/r_file1.png) | *Code part 2*: ![2](img/r_file2.png) |
| *Code part 3*: ![3](img/r_file3.png) | *Code part 4*: ![4](img/r_file4.png) |
| *RStudio Console Start*: ![start](img/r_console_start.png) | *RStudio Console End*: ![end](img/r_console_end.png) |
| *Screenshot of Titanic Age Distribution Graph*: ![graph](img/r_graph1.png) | *Screenshot of Titanic Age Distribution Boxplot*: ![boxplot](img/r_graph2.png)

----
### learn_to_use_r.R:

*learn_to_use_r.R RStudio environment*: ![test RStudio environment](img/r_test_console1.png)

| | |
|---|---|
|*Screenshot of test graph 1*: ![graph 1](img/r_test_graph1.png) | *Screenshot of test graph 2*: ![graph 2](img/r_test_graph2.png) |

----
### Skill set 13: Sphere Volume Calculator

 *Screenshot of Sphere Volume Calculator in Visual Studios*: ![vs running](img/ss13.png)

----
### Skill set 14: Calculator with Error Handling

| | |
|---|---|
| *Screenshot of Calculator with Error Handling in Visual Studios pt.1*: ![vs running](img/ss14_1.png) | *Screenshot of Calculator with Error Handling in Visual Studios pt. 2*: ![vs running](img/ss14_2.png) |

----
### Skill set 15: Writing/ Reading to a File

*Screenshot of Writing/ Reading to a File in Visual Studios*: ![vs running](img/ss15.png)
