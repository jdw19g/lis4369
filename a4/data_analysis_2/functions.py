import re # re module provides regular expression matching operations similar to Perl
from matplotlib import style
import matplotlib.pyplot as plt
import pandas as pd 
import numpy as np # Numerical Python
np.set_printoptions(threshold=np.inf) # print full NumPy array, no ellipsis

def get_requirements():
    print("Data Analysis 2")
    print("\nProgram Requirements:\n"
        + "1. Run demo.py.\n"
        + "2. If errors, more than likely missing installations.\n"
        + "3. Test Python Pakage Installer: pip freeze\n"
        + "4. Research how to install any missing packages:\n"
        + "5. Create at least three functions that are called by the program:\n"
        + "\ta. main(): calls at least two other functions.\n"
        + "\tb. get_requirements(): displays the program requirements.\n"
        + "\tc. data_analysis_2(): displays results as per demo.py."
        + "6. Display graph as per instructions w/in demo.py\n")
        
def data_analysis_2():
     url = "https://raw.github.com/vincentarelbundock/Rdatasets/master/csv/Stat2Data/Titanic.csv"
     df  = pd.read_csv(url)

     print("***DataFrame composed of three components: index, columns, and data. Data also known as values.***")
     index   = df.index
     columns = df.columns 
     values  = df.values
     
     print("\n1. Print indexes:")
     print(index)
     
     print("\n2. Print columns:")
     print(columns)
     
     print("\n3. Print columns (another way):")
     print(df.columns[:]) # using slicing notation
     
     print("\n4. Print (all) values in array format:")
     print(values)
     
     print("\n5. ***Print component data types:***")
     print("\na) index type:")
     print(type(index))
     # pandas.core.indexes.range.RangeIndex
     
     print("\nb) column type:")
     print(type(columns))
     # pandas.core.indexes.base.Index
     
     print("\nc) values type:")
     print(type(values))
     # numpy.ndarray
     
     print("\n6. Print summary of DataFrame (similar to 'describe tablename;' in MySQL):")
     print(df.info())
     
     print("\n7. First five lines (all columns):")
     print(df.head())
     
     # Note: 'Unnamed: 0' appears to be used just to numnber rows
     df = df.drop('Unnamed: 0', 1) # drop columns 'Unnamed: 0'
     print("\n8. Print summary of DataFrame (after dropping column 'Unnamed: 0'):")
     print(df.info())
     
     print("\n9. First five lines (after dropping column 'Unnamed: 0'):")
     print(df.head())
     
     print("\n***Precise data selection (data slicing):***")
     print("\n10. Using iloc return first 3 rows:")
     print(df.iloc[:3]) 
     # print(df.iloc[0:3:1]) # eqivalent to above (slice notation = start:stop:step)
     
     print("\n11. Usinf iloc return last 3 rows (start on index 1310 to end):")
     print(df.iloc[1310:])
     
     # select rows and columns simultaneously
     print("\n12. Select rows 1, 3, and 5; and columns 2, 4, and 6 (includes index column):")
     a = df.iloc[[0, 2, 4], [1, 3, 5]]
     print(a)
     
     print("\n13. Select all rows; and columns 2, 4, and 6 (includes index column):")
     a = df.iloc[:, [1, 3, 5]]
     print(a)
     
     print("\n14. Select rows 1, 3, and 5; and all columns (includes index column):")
     a = df.iloc[[0, 2, 4], :]
     print(a)
     # same as above except leaving out a colon selects all columns as well
     # a = df.iloc[[0, 2, 4]]
     # print(a)
     
     print("\n15. Selct all rows and all columns (includes index column). Note: only first and last 30 records displayed:")
     a = df.iloc[:,:]
     print(a)
     
     print("\n16. Select all rows and columns starting at column 2 (includes index column). Note: only first and last 30 records displayed:")
     a = df.iloc[:,1:]
     print(a)
     
     print("\n17. Select 1 row and 1 column (include index column):")
     a = df.iloc[0:1, 0:1]
     print(a)
     
     print("\n18. Select rows 3-5 and columns 3-5 (include index column):")
     a = df.iloc[2:5, 2:5]
     print(a)
     
     print("\n19. ***Convert pandas DataFrame df to NumPy ndarray, use values command:***")
     b = df.iloc[:, 1:].values # ndarray = N-dimiensional array (rows and columns)
     
     print("\n20. Print data frame type:")
     print(type(df))
     
     print("\n21. Print a type:")
     print(type(a))
     
     print("\n22. Print b type:")
     print(type(b))
     
     print("\n23. Print number of dimensions and items in array (rows, columns). Remember: starting at column 2:")
     print(b.shape)
     
     print("\n24. Print type of items in array. Remember: ndarray is an array of arrays. each record/ item is an array.")
     print(b.dtype)
     
     print("\n25. Printing a:")
     print(a)
     
     print("\n26. Length a:")
     print(a)
     
     print("\n27. Printing b:")
     print(b)
     
     print("\n28. Length b:")
     print(b)
     
     # Print element of ndarray b in *second* row, *third* column
     print("\n29. Print element of (NumPy array) ndarray b in *second* row, *third* column:")
     print(b[1,2])
     
     # print full NumPy array, no ellipsis: here is why np.set_printoptions(threshold=np.inf) is set at the top of file
     print("\n30. Print all records for NumPy array column 2:")
     print(b[:,1])
     
     print("\n31. Get passanger names:")
     names = df["Name"]
     print(names)
     
     print("\n32. Find all passengers with the name 'Allison' (using regular expressions):")
     # r = \'(Allison)\'
     for name in names:
         print(re.search(r'(Allison)', name))
     
     print("\n***33. Statistical Analysis (DataFrame notation):***")
     print("\na) Print mean age:")
     avg = df["Age"].mean() # second column
     print(avg)
     
     print("\nb) Print mean age, rounded to two decimal places:")
     avg = round(df["Age"].mean(), 2) # will not display last 0
     print(avg)
     
     print("\nc) Print mean of every column in DataFrzme (may not be suitable with certain columns):")
     avg_all = df.mean(axis=0) # mean every column
     # avg_all = df.mean(axis=1) # mean every row
     print(avg_all)
     
     print("\nd) Print summary statistics (DataFrame notation):")
     # returns three quartiles, mean, count, min/max values, and standard deviation
     describe = df["Age"].describe() # second column
     # describe = df["Age"].describe(percentiles=[.10, .20, .50, .80]) # choose different percentile
     print(describe)
     
     print("\ne) Print minimum age (DataFrame notation)")
     # can also do functions seperately
     min = df["Age"].min() # second column
     print(min)
     
     print("\nf) Print maximum age (DataFrame notation):")
     max = df["Age"].max() # second column
     print(max)
     
     print("\ng) Print mediun age (DataFrame notation):")
     median = df["Age"].median() # second column
     print(median)
     
     print("\nh) Print mode age (DataFrame notation):")
     mode = df["Age"].mode() # second column
     print(mode)
     
     print("\ni) Print number of values (DataFrame notation):")
     count = df["Age"].count() # second column
     print(count)
     
     print("\n***Graph: Display ages of the first 20 passengers (usecode from previous assignment):***")
     style.use('ggplot')
     
     df = df.head(20)
     
     df['Age'].plot()
     plt.legend()
     plt.show()