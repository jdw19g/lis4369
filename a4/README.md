# LIS 4369 - Extensible Enterprise Solutions
----
## jdw19g / James White
----
### Assignment 4 Requirements:

1. Code and run demo.py.
2. Use demo.py to backward-engineer the program.
3. The program should be organized with three functions:

    a) main.py:

        i) main(): Calls at least two other functions.

    b) functions.py:

        i) get_requirements(): displays the program requirements.

        ii) data_analysis_2(): displays results as per demo.py.

4. Create *A4 Data Analysis 2* application.
5. Create *A4 Data Analysis 2* Jupyter Notebook.
6. Skill set 10: Python Dictionaries
7. Skill set 11: Pseudo-Random Number Generator
8. Skill set 12: Temperature converter
9. Provide screenshots of all work.
10. Bitbucket Repo Links:
    
    a) https://bitbucket.org/jdw19g/lis4369

#### README.md file should include the following items:

* Screenshot of data analysis 2 application running in Visual Studio Code.

* Link to A4 .ipynb file: [data_analysis_2.ipynb](data_analysis_2/data_analysis_2.ipynb "A4 Jupyter Notebook")

* Screenshots of skill sets.

#### Assignment Screenshots:
----
### Data analysis:

| | |
|---|---|
| *Screenshot of Data analysis main module*: ![Data analysis main.py](img/main.png) | *Screenshot of Data analysis functions module pt. 1*: ![Data analysis functions.py](img/function1.png) |
| *Screenshot of Data analysis functions module pt. 2*: ![Data analysis functions.py](img/function2.png) | *Screenshot of Data analysis functions module pt. 3*: ![Data analysis functions.py](img/function3.png) |
| *Screenshot of Data analysis functions module pt. 4*: ![Data analysis functions.py](img/function4.png) | *Screenshot of Data analysis output pt. 1*: ![Data analysis output](img/output1.png) |
| *Screenshot of Data analysis output pt. 2*: ![Data analysis output](img/output2.png) | *Screenshot of Data analysis graph*: ![Data analysis graph](img/graph.png)

----
### Data Analysis II Jupyter Notebook:

| | |
|---|---|
|*Screenshot of Jupyter Notebook*: ![jupyter notebook pt.1](img/jp1.png) | *Screenshot of Jupyter Notebook cont.*: ![jupyter notebook pt.2](img/jp2.png) |
|*Screenshot of Jupyter Notebook*: ![jupyter notebook pt.3](img/jp3.png) | *Screenshot of Jupyter Notebook cont.*: ![jupyter notebook pt.4](img/jp4.png) |

----
### Skill set 10: Python Dictionaries

 *Screenshot of lists in Visual Studios*: ![vs running](img/ss10_o.png)

----
### Skill set 11: Pseudo-Random Number Generator

 *Screenshot of tuples in Visual Studios*: ![vs running](img/ss11_o.png)
----
### Skill set 12: Temperature converter

*Screenshot of sets in Visual Studios*: ![vs running](img/ss12_o.png)
