def get_requirements():
    print("Developer: James D. White")
    print("Python Lists")
    print("\nProgram Requirements:\n"
         + "1. Lists (Python data structure): mutable, ordered sequence of elements.\n"
         + "2. Lists are mutable/changable--that is, can insert, update, delete.\n"
         + "3. Creat list - using square brackets [list]: my_list = [""cherries"", ""apples"", ""bananas"", ""oranges""].\n"
         + "4. Create a program that mirros the following IPO (input/process/output) formatw.\n"
         + "Note: user enters number of requested list elements, dynamically rendered below (that is, number of elements can change each run).\n")

def user_input():
    # initialize variables and list
    num = 0

    # input user data
    print("Input:")
    num = int(input("Enter number of list elements: "))
    return num

    print() # print blank line

def using_lists(num):
    # process
    my_list = [] # creates empty list
    # range(stop)
    # stop: Number of integers (whole numberes) to generate,
    # starting from zero; e.g., range(3) == [0,1,2]
    for i in range(num): # run loop num times (0 to num)
        # prompt user for list element
        my_element = input('Please enter list element' + str(i + 1) + ": ")
        my_list.append(my_element) # append each element at end of list

# Output
    print("\nOutput:")
    print("Print my_list:")
    print(my_list)

    elem = input("\nPlease enter list element: ")
    pos = int(input("Please enter list *index* position ( note: must be converted to int): "))

    print("\nInsert element into specific position in my_list")
    my_list.insert(pos, elem) # Note: pos 1 inserts item into second element
    print(my_list)
    
    print("\nCount number of elements in list: ")
    # also works for strings, tuples, and dict objects
    print(len(my_list))

    print("\nSort elements in list alphabetically: ")
    my_list.sort() # sort alphabetically
    print(my_list)

    print("\nReverse list: ")
    my_list.reverse() # reverse list
    print(my_list)

    print("\nRemove last list element: ")
    my_list.pop() # delete last element
    print(my_list)

    print("\nDelete second element from list by *index* (note: 1 = 2nd element): ")
    # pops element at index specified
    # my_list.pop(1)
    # or...
    del my_list[1]
    print(my_list)

    print("\nDelete element from list by *value* (cherries): ")
    my_list.remove('cherries')
    print(my_list)

    print("\nDelete all elements from the list: ")
    my_list.clear() # deletes all the elements
    print(my_list)

    # or...
    # print("\nDelete all elements from list(using slicing): ")
    # del my_list[:] # deletes all elements using slicing
    #print(my_list)