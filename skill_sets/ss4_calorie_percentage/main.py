import functions as f

def main():
    f.get_requirements()
    user_input = f.get_user_input()

    # For testing purposes: print and quit (halts program execution)
    # print(user_input)
    # quit

    # tuple unpacking: tuple values unpacked into variable names
    fat, carb, protein = user_input

    # pass user-entered values
    f.calculate_calories(fat, carb, protein)

if __name__ == "__main__":
    main()