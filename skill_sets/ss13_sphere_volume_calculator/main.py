import functions as f


def main():
    f.get_requirements()
    f.sphere_volume()


if __name__ == "__main__":
    main()