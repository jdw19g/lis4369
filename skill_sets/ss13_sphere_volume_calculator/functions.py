import math

def get_requirements():
    print("Developer: James D. White")
    print("Sphere Volume Program\n")
    print("\nProgram Requirements:\n"
         + "1. Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inches.\n"
         + "2. Must use Python's *built-in* PI and pow() capabilities.\n"
         + "3. Program checks for non integers and non numeric values.\n"
         + "4. Program continues to prompt for user entry until no longer requested, prompt accepts upper and lower case letters.\n")


def sphere_volume():
    # initialize variables
    diameter = 0
    volume   = 0.0
    gallons  = 0.0
    choice   = '' # initialize to space character

    # IPO: Input > Process > Output
    # get user data
    print("Input:")

    choice = input(
        "Do you want to calculate a sphere volume (y or n)? ").lower()

    # Process and output
    print("\nOutput:")

    # Built-in excweptions
    """ TyperError vs ValueError:
    Passing arguments of the wrong type (e.g. passing a list when an int is expected) should result in a TypeError, 
    but passing arguments with the wrong value (e.g. a number outside expected boundaries) should result in a ValueError.
    Below, ValueError exception testing is used below to test for *any* non-integer value.
    """

    while (choice[0] == 'y'):
        diameter = input("Please enter diameter in inches: ")
        test = True
        while (test == True):
            try:
                diameter = int(diameter)
                # Note: *must* use floating point values, otherwise, integer division (i.e. incorrect result)
                # returns cubic inches
                volume  = ((4.0/3.0) * math.pi * math.pow(diameter/2.0,3))
                gallons = volume/231 #convert cubic inches to gallons
                print(
                    "\nSphere volume: {0:,.2f} liquid U.S. gallons\n".format(gallons))
                test = False

            except ValueError:
                print("\nNot a valid integer!")
                diameter = input("Please enter diameter in inches: ")
            continue
    
        choice = input(
            "Do you want to calculate another sphere volume (y or n)? ").lower()
    
    print("\nThank you for using our Sphere Volume Calculator")



