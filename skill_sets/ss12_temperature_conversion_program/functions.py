def get_requirements():
    print("Developer: James D. White")
    print("Temperature Conversion Program")
    print("\nProgram Requirements:\n"
         + "1. Program converts user-entered temperature into Fahrenheit or Celsisu scales.\n"
         + "2. Program continues to promt the user entry until no longer requested.\n"
         + "3. Note: upper or lower case letters permitted. Though, incorrect entties are not permitted.\n"
         + "4. Note: Program does not validate numeric data (optional requirement).\n")


def temperature_conversion():
    # initialize variables
    temperature = 0.0
    choice = '' # initialize to space character
    type = '' # initialize to space character

    # IPO: Input > Process > Output
    # Get user data
    print("Input:")

    choice = input("Do you want to convert a temperature (y or n)? ").lower()

    # Process and Output:
    print("\nOutput:")

    while (choice[0] == 'y'):
        type = input(
            "Fahrenheit to Celcius? Type \"f\", or Celcius to Fahrenheit? Type \"c\": ").lower()
        
        if type[0] == 'f':
            temperature = float(input("Enter temperature in Fahrenheit: "))
            temperature = ((temperature - 32)*5)/9
            print("Temperature in Celcius = " + str(temperature))
            choice = input(
                "\nDo you want to convert another temperature (y or n)? ").lower()
        
        elif type[0] == 'c':
            temperature = float(input("Enter temperature in Celcius: "))
            temperature = (temperature * 9/5) + 32
            print("Temperature in Fahrenheit = " + str(temperature))
            choice = input(
                "\nDo you want to convert another temperature (y or n)? ").lower()

        else:
            print("Incorrect entry. Please try again.")
            choice = input(
                "\nDo you want to convert a temperature (y or n)? ").lower()
        
    print("\nThank you for using our temperature conversion program!")