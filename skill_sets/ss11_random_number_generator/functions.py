import random

def get_requirements():
    print("Developer: James D. White")
    print("Pseudo-Random Number Generator")
    print("\nProgram Requirements:\n"
         + "1. Get user beginning and ending integer values, and store two variables.\n"
         + "2. Display 10 pseudo-random numbers between, and including, above values.\n"
         + "3. Must use integer data types.\n"
         + "4. Example 1: Using range() and randint() functions.\n"
         + "5. Example 2: Using a list with range() and shuffle() functions.\n")


def random_numbers():
    # initialize variables
    start = 0
    end = 0

    # IPO: Input > Process > Output
    # get user data
    print("Input:")
    start = int(input("Enter beginning value: "))
    end = int(input("Enter ending value: "))

    # Process and Output
    # diasplay 10 random numbers between start and end, inclusive
    # multi-line docstrings to print mult-line comments:
    """ print() Parameters:
    Generic syntax: print(*objects, sep='', end='\n', file=sys.stdout, flush=False)
    objects: object(s) ro be printed. * indicates may be more than one object. multiple comma-seperated objects or single object.
    sep: objects seperated by sep. Default value:'' (space)
    end: printed at last. Default: new line character (\n)
    file: must be object with write(string) method. If pmitted, sys.stdout used as default, prints to screen.
    flush - if True, stream is forcibly flushed. Default value: False
    """

    print("\nOutput:")
    print("Example 1: Using range() and randint() functions:")
    for count in range(10):
        print(random.randint(start, end), sep=", ", end=" ")

    print()

    print("\nExample 2: Using a list, with range() and shuffle() functions:")
    # shuffle() randomizes items of a list in place (must include list)
    # range generates list of integers from 1, but not including, 11
    r = list(range(start, end +1)) # because list (zero-based) must add 1
    random.shuffle(r)
    for i in r:
        print(i, sep=", ", end=" ")

    print()

