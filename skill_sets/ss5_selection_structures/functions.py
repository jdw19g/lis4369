def get_requirements():
    print("Developer: James D. White")
    print("Python Selection Structures")
    print("\nProgram Requirements:\n"
         + "1. Use python selection structure.\n"
         + "2. Prompt user for two numbers, and a suitable operator.\n"
         + "3. Test for corrct numeric operator.\n"
         + "4. Replicate display below.\n")
    
    print("Python Calculator")

def get_user_input():
    # initialize variables
    num1 = 0.0
    num2 = 0.0

    num1 = float(input("Enter num1: "))
    num2 = float(input("Enter num2: "))
    print("\nSuitable Operators: +, -, *, /, // (integer division), % (modulo operator), **(power)")
    op   = input("Enter operator: ")

    return num1, num2, op

    # below works, *but* should be called from invoking program(i.e. return back to main())
    # print_selection_structures(num1, num2, op)

def print_selection_structures(num1, num2, op):
    # selection structures

    # if..else if...else if...else

    # if condition 1:
    #    indented statement block for first true condition
    # elif condition 2:
    #    indented statement block for first true condition
    # elif condition 3:
    #    indented statement block for first true condition
    # else:
    #    execute if all expressions above are false

    if op == "+":
        print(num1 + num2)
        # or
        # print("{w}{x}{y} = {z}"format(w=num1, x=op, y=num2, z=num1 + num2))

    elif op == "-":
        print(num1 - num2)
    elif op == "*":
        print(num1 * num2)
    elif op == "/":
        if num2 == 0:
            print("Cannot divide by zero!")
        else:
            print(num1 / num2)
    elif op == "//":
        if num2 == 0:
            print("Cannot divide by zero!")
        else:
            print(num1 // num2)
    elif op == "%":
        if num2 == 0:
            print("Cannot divide by zero!")
        else:
            print(num1 % num2)
    elif op == "**":
        # use str() to concat numbers w/strings
        # print("Using ** operator: " + str(num1 ** num2))
        # or...
        print("Using pow() function: " + str(pow(num1, num2)))
    else:
        priont("Incorrect operator!")
