> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## jdw19g / James White

### Assignment 1 Requirements:

*Four parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installation
3. Questions
4. Bitbucket Repo Links:
    
    a) https://bitbucket.org/jdw19g/lis4369
    
    b) https://bitbucket.org/jdw19g/bitbucketstationlocations

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator application running

* Link to A1 .ipynb file: [tip_calculator.ipynb](a1_tip_calculator/tip_calculator.ipynb "A1 Jupyter Notebook")

* git commands w/short descriptions

> This is a blockquote.
>
>This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: Create an empty Git repository or reinitialize an existing one
2. git status: Show the working tree status
3. git add: Add file contents to the index
4. git commit: Record changes to the repository
5. git push: Update remote refs along with associated objects
6. git pull: Fetch from and integrate with another repository or a local branch
7. git help: Display help information about Git

#### Assignment Screenshots:

*Screenshot of tip calculator code*:

![Tip calculator code](img/tip_calc_code.png)

*Screenshot of tip calculator running in IDLE*:

![Tip calculator in IDLE](img/tip_calc_IDLE.png)

*Screenshot of tip calculator running in Visual Studio Code:

![Tip calculator in Visual Studio Code](img/tip_calc_VSC.png)

*Screenshot of tip calculator in Jupyter Notebook:*

![Tip calculator in Jupyter Notebook](img/tip_calc_jupyter.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jdw19g/bitbucketstationlocations/ "Bitbucket Station Locations")