# LIS 4369 - Extensible Enterprise Solutions
----
## jdw19g / James White
----
### Project 2 Requirements:

1. Use Assignment 5 screenshots and R Manual to backward-engineer the following requirements:
2. Resources:
    * a. R Manual: https://cran.r-project.org/doc/manuals/r-release/R-lang.pdf
    * b. R for Data Science: https://r4ds.had.co.nz/ 
3. Use Motor Trend Car Road Tests data:
    * a. Research the data! https://stat.ethz.ch/R-manual/R-devel/library/datasets/html/mtcars.html
    * b. url = "http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv" 
4. Provide screenshots of all work.
5. Bitbucket Repo Links:
    
    a) https://bitbucket.org/jdw19g/lis4369

#### README.md file should include the following items:

* Screenshot of lis4369_p2.R running in RStudio.

#### Assignment Screenshots:
----
### lis4369_p2.R:

*lis4369_p2.R RStudio Environment*: ![RStudio environment](img/r_env.png)

| | |
|---|---|
| *Code part 1*: ![1](img/code1.png) | *Code part 2*: ![2](img/code2.png) |
| *Code part 3*: ![3](img/code3.png) | *Code part 4*: ![4](img/code4.png) |
| *Code part 5*: ![5](img/code5.png) |  |

----
### lis4369_p2.R graphs:

| | |
|---|---|
|*Graph *: ![graph comparing displacement and MPG](img/plot_disp_and_mpg_1.png) | *Graph comparing weight and MPG*: ![graph 2](img/plot_disp_and_mpg_2.png) |

----
### lis4369_p2.R Output:

| | |
|---|---|
| *Output part 1*: ![1](img/output1.png) | *Output part 2*: ![2](img/output2.png) |
| *Output part 3*: ![3](img/output3.png) | *Output part 4*: ![4](img/output4.png) |
| *Output part 5*: ![5](img/output5.png) | *Output part 6*: ![6](img/output6.png) |
| *Output part 7*: ![7](img/output7.png) | *Output part 8*: ![8](img/output8.png) |


----

