import functions as f


def main():

    f.get_requirements()
    check = "y"
    while check.lower() == "y":
        f.estimate_painting_cost()
        print()
        check = input("Estimate another paint job? (y/n): ")
        print()

    print("Thank you for using my Painting Estimator!")
    print("Please see my bitbucket:  https://bitbucket.org/jdw19g/lis4369/")

if __name__ == "__main__":
    main()