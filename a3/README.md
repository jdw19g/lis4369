# LIS 4369 - Extensible Enterprise Solutions
----
## jdw19g / James White
----
### Assignment 3 Requirements:

1. Backward-engineer (using Python) a Paint Estimator.
2. The program should be organized with two modules:

    a) main.py:

        i) main()

    b) functions.py:

        i) get_requirements()

        ii) estimate_painting_cost()

        iii) print_painting_estimate()

        iv) print_painting_percentage()

3. Create *A3 Paint Estimator* application.
4. Create *A3 Paint Estimator* Jupyter Notebook.
5. Skill set 4: Calorie Percentage calculator
6. Skill set 5: Python selection structure practice
7. Skill set 6: Python loops practice
8. Provide screenshots of all work.
9. Bitbucket Repo Links:
    
    a) https://bitbucket.org/jdw19g/lis4369

#### README.md file should include the following items:

* Screenshot of Paint Estimator application running in both Idle and Visual Studio Code.

* Link to A3 .ipynb file: [paint_estimate.ipynb](paint_estimate/paint_estimate.ipynb "A3 Jupyter Notebook")

* Screenshots of skill sets.

#### Assignment Screenshots:
----
### Paint Estimator:

| | |
|---|---|
| *Screenshot of Paint Estimator main module*: ![Paint estimator main.py](img/a3_main.png) | *Screenshot of Paint Estimator functions module*: ![Paint estimator functions.py](img/a3_functions.png) |
| *Screenshot of Paint Estimator in Visual Studios*: ![Paint estimator vs](img/a3_vs.png) | *Screenshot of Paint Estimator in IDLE*: ![Paint estimator idle](img/a3_idle.png) |

----
### Paint Estimator Jupyter Notebook:

| | |
|---|---|
|*Screenshot of Jupyter Notebook*: ![jupyter notebook pt.1](img/a3_jp1.png) | *Screenshot of Jupyter Notebook cont.*: ![jupyter notebook pt.2](img/a3_jp2.png) |

----
### Skill set 4: Calorie Percentage Calculator

| | |
|---|---|
| *Screenshot of Calculator in Visual Studios*: ![vs running](img/ss4_vs.png) | *Screenshot of calculator in IDLE*: ![idle running](img/ss4_idle.png) |

----
### Skill set 5: Python Selection Structures

| | | |
|---|---|---|
| *Screenshot of addition*: ![addition](img/ss5_add.png) | *screenshot of subtraction*: ![subtraction](img/ss5_subtract.png) | *Screenshot of multiplication*: ![multiply](img/ss5_multiply.png) |
| *Screenshot of division*: ![division](img/ss5_division.png) | *Screenshot of integer division*: ![integer division](img/ss5_integer_division.png) | *Screenshot of modulo*: ![modulo](img/ss5_modulo.png) |
| *Screenshot of power*: ![power](img/ss5_power.png) | | |

----
### Skill set 6: Python Loops:

*Screenshot of loops*: ![loops](img/ss6_loops.png)
