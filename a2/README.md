> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## jdw19g / James White

### Assignment 2 Requirements:

*Four parts:*

1. Backward-engineer (using Python) a Payroll Calculator
2. The program should be organized with two modules
    a) functions.py module contains the following functions:
        i) get_requirements()
        ii) calculate_payroll()
        iii) print_pay()
    b) main.py module imports the functions.py module, and calls the functions. 
3. Be sure to test your program using both IDLE and Visual Studio Code.
4. Questions
6. Skill sets completed
5. Bitbucket Repo Links:
    
    a) https://bitbucket.org/jdw19g/lis4369

#### README.md file should include the following items:

* Screenshot of Payroll application application running in both Idle and Visual Studio Code.

* Link to A2 .ipynb file: [payroll_calc.ipynb](payroll_calc/payroll_calc.ipynb "A2 Jupyter Notebook")

* Screenshots of skill sets.

#### Assignment Screenshots:

| | |
|---|---|
| *Screenshot of Payroll calculator main module*:     ![Payroll calculator main.py](img/a2_main.png) | *Screenshot of Payroll calculator functions module*: ![Payroll calculator functions.py](img/a2_functions.png) |
| *Screenshot of Payroll calculator running in VS Code*: ![Payroll](img/a2_terminal_payroll.png) | *Screenshot of Payroll calculator running in VS code (overtime)*: ![Payroll calculating overtime](img/a2_terminal_payroll_overtime.png) |
| *Screenshot of Payroll calculator running in IDLE*: ![Payroll](img/a2_idle_payroll.png) | *Screenshot of Payroll calculator running in IDLE (overtime)*: ![Payroll calculating overtime](img/a2_idle_payroll_overtime.png) |
| *Screenshot of Payroll calculator in Jupyter Notebook*: ![Jupyter Notebook pt.1](img/a2_jn_pt1.png) | *Screenshot of Payroll calculator in Jupyter Notebook*: ![Jupyter Notebook pt.2](img/a2_jn_pt2.png) |
| *Screenshot of skill set 1*: ![Skill set 1](img/ss1.png) | *Screenshot of skill set 2*:     ![Skill set 2](img/ss2.png) |
|*Screenshot of skill set 3*: ![Skill set 3](img/ss3.png) | |
