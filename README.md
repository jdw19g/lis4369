> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4369 - Extensible Enterprise Solutions (Python)

## James White / jdw19g

### LIS 4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Install Python
    * Install R
    * Install R Studio
    * Install Visual Studio Code
    * Create *a1_tip_calculator* application
    * Create *a1 tip calculator* Jupyter Notebook
    * Provide Screenshots of installations
    * Create Bitbucket Repo
    * Complete Bitbucket tutorial (bitbucketstationlocations)
    * provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Backward-engineer (using Python) a Payroll Calculator
    * The program should be organized with two modules
        * main.py
        * functions.py
    * Be sure to test your program using both IDLE and Visual Studio Code.
    * Create *A2 Payroll calculator* application
    * Create *A2 Payroll calculator* Jupyter Notebook

3. [A3 README.md](a3/README.md "My A3 README.md file")
    * Backward-engineer (using Python) a Paint Estimator.
    * The program should be organized with two modules:
        * main.py :

            i) main()

        * functions.py :

            i) get_requirements()

            ii) estimate_painting_cost()

            iii) print_painting_estimate()

            iv) print_painting_percentage()

    * Create *A3 Paint Estimator* application.
    * Create *A3 Paint Estimator* Jupyter Notebook.
    * Skill set 4: Calorie Percentage calculator
    * Skill set 5: Python selection structure practice
    * Skill set 6: Python loops practice
    * Provide screenshots of all work.

4. [A4 README.md](a4/README.md "My A4 README.md file")
    * Code and run demo.py.
    * Use demo.py to backward-engineer the program.
    * The program should be organized with three functions:
        * main.py :

            i) main(): Calls at least two other functions.

        * functions.py :

            i) get_requirements(): displays the program requirements.

            ii) data_analysis_2(): displays results as per demo.py.

    * Create *A4 Data Analysis 2* application.
    * Create *A4 Data Analysis 2* Jupyter Notebook.
    * Skill set 10: Python Dictionaries
    * Skill set 11: Pseudo-Random Number Generator
    * Skill set 12: Temperature converter
    * Provide screenshots of all work.

5. [A5 README.md](a5/README.md "My A5 README.md file")
    * Complete the Introduction to R setup and the tutorial.
    * Code demo1.r and demo2.r to create lis4369_a5.r
    * Using the RStudio environment code and run lis4369_a5.r.
    * Skill set 13: Sphere Volume Calculator.
    * Skill set 14: Calculator with Error Handling.
    * Skill set 15: Writing/ Reading to a file.
    * Provide screenshots of all work.

6. [P1 README.md](p1/README.md "My P1 README.md file")
    * Create and use a data analysis tool.
    * The program should be organized with two modules:
         * main.py :

            i) main(): Calls at least two other functions.

        * functions.py :

            i) get_requirements(): displays the program requirements.

            ii) data_analysis_1(): displays the following data.
    * Create *P1 Data Analysis 1* application.
    * Create *P1 Data Analysis 1* Jupyter Notebook.
    * Skill set 7: Python Lists
    * Skill set 8: Python Tuples
    * Skill set 9: Python Sets
    * Provide screenshots of all work.


7. [P2 README.md](p2/README.md "My P2 README.md file")
    * Use Assignment 5 screenshots and R Manual to backward-engineer the following requirements:
    * Resources:
        * a. R Manual: https://cran.r-project.org/doc/manuals/r-release/R-lang.pdf
        * b. R for Data Science: https://r4ds.had.co.nz/ 
    * Use Motor Trend Car Road Tests data:
        * a. Research the data! https://stat.ethz.ch/R-manual/R-devel/library/datasets/html/mtcars.html
        * b. url = "http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv" 
    * Provide screenshots of all work.

