# LIS 4369 - Extensible Enterprise Solutions
----
## jdw19g / James White
----
### Project I Requirements:

1. Create and use a data analysis tool.
2. The program should be organized with two modules:

    a) main.py:

        i) main()

    b) functions.py:

        i) get_requirements(): displays the program requirements.

        ii) data_analysis_1(): displays the following data.

3. Create *P1 Data Analysis 1* application.
4. Create *P1 Data Analysis 1* Jupyter Notebook.
5. Skill set 7: Python Lists
6. Skill set 8: Python Tuples
7. Skill set 9: Python Sets
8. Provide screenshots of all work.
9. Bitbucket Repo Links:
    
    a) https://bitbucket.org/jdw19g/lis4369

#### README.md file should include the following items:

* Screenshot of Paint Estimator application running in both Idle and Visual Studio Code.

* Link to P1 .ipynb file: [data_analysis_1.ipynb](data_analysis_1/data_analysis_1.ipynb "P1 Jupyter Notebook")

* Screenshots of skill sets.

#### Assignment Screenshots:
----
### Data analysis:

| | |
|---|---|
| *Screenshot of Data analysis main module*: ![Data analysis main.py](img/vs_main.png) | *Screenshot of Data analysis functions module*: ![Data analysis functions.py](img/vs_functions.png) |
| *Screenshot of Data analysis in Visual Studios*: ![Paint estimator vs](img/vs_output.png) | *Screenshot of Data analysis graph*: ![Data analysis graph](img/vs_graph.png) |

----
### Paint Estimator Jupyter Notebook:

| | |
|---|---|
|*Screenshot of Jupyter Notebook*: ![jupyter notebook pt.1](img/jp1.png) | *Screenshot of Jupyter Notebook cont.*: ![jupyter notebook pt.2](img/jp2.png) |
|*Screenshot of Jupyter Notebook*: ![jupyter notebook pt.3](img/jp3.png) | *Screenshot of Jupyter Notebook cont.*: ![jupyter notebook pt.4](img/jp4.png) |

----
### Skill set 7: Python Lists

 *Screenshot of lists in Visual Studios*: ![vs running](img/ss7.png)

----
### Skill set 8: Python Tuples

 *Screenshot of tuples in Visual Studios*: ![vs running](img/ss8.png)
----
### Skill set 9: Python Sets:

*Screenshot of sets in Visual Studios*: ![vs running](img/ss9.png)
