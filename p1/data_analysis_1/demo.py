# Pandas = "Python Data Analysis Library"
# Be sure to: pip install pandas-datareader

import datetime
import pandas_datareader as pdr 
import matplotlib.pyplot as plt 
from matplotlib import style

start = datetime.datetime(2010, 1, 1)
end = datetime.datetime(2018, 10, 15)
df = pdr.DataReader("XOM", "yahoo", start, end)

print("\nPrint number of records: ")


print(df.columns)

print("\nPrint data frame: ")
print(df) # Note: for efficiency, only prints 60--not *all* records

print("\nPrint first five lines:")
    # Note: "Date" is lower than the other columns as it is treated as an index
print(df.head()) # head() prints top 5 rows. Here, with 7 columns

print("\nPrint last five lines:")
print(df.tail())

print("\nPrint first two lines:")
print(df.head(2))
    
print("\nPrint last two lines:")
print(df.tail(2))

    # Research what these styles do!
    # style.use('fivethirtyeight')
    # compare with...
style.use('ggplot')

df['High'].plot()
df['Adj Close'].plot()
plt.legend()
plt.show()
